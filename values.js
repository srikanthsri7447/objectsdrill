function values(obj){
    if(!obj){
        return []
    }
    else{

        let output = []

        for(let item in obj){
            output.push(obj[item])
        }

        return output;
    }
}

module.exports = values;