function invert(obj){
    if(!obj){
        return []
    }
    else{
        let output = {}

        for(let item in obj){
            output[obj[item]] = item
        }
        
        return output
    }
}

module.exports = invert;