function mapObject(obj, cb){
    if(!obj || !cb){
        return []
    }
    else{
        let output = {}

        for(let item in obj){
            output[item] = cb(obj[item])
        }

        return output
    }

}

module.exports = mapObject;