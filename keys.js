function keys(obj){
   
    if(!obj){
        return []
    }
    else{
        
        let output = []

        for (const item in obj) {
            output.push(obj[item]);
          }
        
        return output;
    }
}


module.exports = keys;