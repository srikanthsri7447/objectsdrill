const invert = require('../invert');

const testObject = {Moe: "Moses", Larry: "Louis", Curly: "Jerome"};

const result = invert(testObject);

console.log(result)
