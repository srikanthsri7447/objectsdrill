function defaults(obj, defaultProps){
    if(!obj || !defaultProps) return [];
    else {
        let output = {}

        for(let item in defaultProps){
            if(!obj[item]){
                output[item] = defaultProps[item]
            }
            else{
                output[item] = obj[item]
            }
        }

        return output
    }
}

module.exports = defaults;